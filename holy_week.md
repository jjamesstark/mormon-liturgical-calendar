# Holy Week 

## Palm Sunday (Passion Sunday)

One week before Easter; The triumphal entry into Jerusalem 
- Matthew 21:1-11
- Mark 11:1-11
- Luke 19:28-44
- John 12:12-19

## Holy Monday & Holy Tuesday

Not much agreement over the timeline here. Most common narratives include

- cursing the fig tree
- the questioning of Jesus' authority
- the cleansing of the temple

As well as some selected parables (would be worth picking some from the chapters including the above narratives)

## Spy Wednesday

The betrayal | Matthew 26:3-16; Mark 14:1-12; Luke 22:1-6
- Sanhedrin gathers and conspires | Matthew 26:3–5; Mark 14:1–2; Luke 22:1–2
- Mary anoints Jesus with costly oil | Matthew 26:6–9; Mark 14:3-5; John 12:3–6
- Judas offers to betray Jesus in exchange for money |  Matthew 26:14–16; Mark 14:10–12; Luke 22:3–6

## Maundy Thursday

The last supper and sacrament. | Matthew 26:17-30; Mark 14:12-26; Luke 22:7-39
- A description of the Passover celebration | Exodus 12:1-8, 11-14
- First written account | 1 Corinthians 11:23-26 (first account written)
- Jesus washing his disciples' feet | John 13:1-15

## Good Friday
```
===================================================================
this section will need more study to create the appropriate content
===================================================================
```
The suffering in the garden, arrest and crucifixion of Jesus

## Black Saturday
```
===================================================================
this section will need more study to create the appropriate content
===================================================================
```

Jesus' body in the tomb and the Harrowing of Hell. Latter-day Saints have a unique belief around the Harrowing of Hell that would be good to highlight. 

### Easter Vigil

From [wikipedia](https://en.wikipedia.org/wiki/Holy_Week#Holy_Saturday_(Black_Saturday))

```
===================================================================
this section will need more study to create the appropriate content
===================================================================
```

>The Liturgy begins after sundown on Holy Saturday as the crowd gathers inside the unlit church. In the darkness (often in a side chapel of the church building or, preferably, outside the church), a new fire is kindled and blessed by the priest. This new fire symbolizes the light of salvation and hope that God brought into the world through Christ's Resurrection, dispelling the darkness of sin and death. From this fire is lit the Paschal candle, symbolizing the Light of Christ. This Paschal candle will be used throughout the season of Easter, remaining in the sanctuary of the Church or near the lectern, and throughout the coming year at baptisms and funerals, reminding all that Christ is "light and life."
>
>The candles of those present are lit from the Paschal candle. As this symbolic "Light of Christ" spreads throughout those gathered, the darkness is decreased. A deacon, or the priest if there is no deacon, carries the Paschal Candle at the head of the entrance procession and, at three points, stops and chants the proclamation "The Light of Christ" (until Easter 2011, the official English text was "Christ our Light"), to which the people respond "Thanks be to God." Once the procession concludes with the singing of the third proclamation, the lights throughout the church are lit, except for the altar candles. Then the deacon or a cantor chants the Exultet (also called the "Easter Proclamation"), After that, the people put aside their candles and sit down for the Liturgy of the Word.
>
>The Liturgy of the Word includes between three and seven readings from the Old Testament, followed by two from the New (an Epistle and a Gospel). The Old Testament readings must include the account in Exodus 14 of the crossing of the Red Sea, seen as an antitype of baptism and Christian salvation. Each Old Testament reading is followed by a psalm or canticle (such as Exodus 15:1–18 and a prayer relating what has been read to the Mystery of Christ. After the Old Testament readings conclude, the Gloria in excelsis Deo, which has been suspended during Lent, is intoned and bells are rung
>
>A reading from the Epistle to the Romans is proclaimed. The Alleluia is sung for the first time since the beginning of Lent. The Gospel of the Resurrection then follows, along with a homily.
>
>After the conclusion of the Liturgy of the Word, the water of the baptismal font is blessed and any catechumens or candidates for full communion are initiated into the church, by baptism or confirmation. After the celebration of these sacraments of initiation, the congregation renews their baptismal vows and receive the sprinkling of baptismal water. The general intercessions follow.

## Resurrection Sunday

```
===================================================================
this section will need more study to create the appropriate content
===================================================================
```
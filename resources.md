# Sources for Lists and Calendars

## Catholic

- [2020 USCCB Calendar](http://www.usccb.org/about/divine-worship/liturgical-calendar/upload/2020cal.pdf)
- [2019 USCCB Calendar](http://www.usccb.org/about/divine-worship/liturgical-calendar/upload/2019cal.pdf)
## 2019 - Catholic
| Date | Listing | 
| --- | --- |
| Sunday, December 1, 2019 | Is 2:1-5/Rom 13:11-14/Mt 24:37-44 |
| Monday, December 2, 2019 | Is 4:2-6 (second choice)/Mt 8:5-11  |
| Tuesday, December 3, 2019 | Is 11:1-10/Lk 10:21-24 |
| Wednesday, December 4, 2019 | Is 25:6-10a/Mt 15:29-37 |
| Thursday, December 5, 2019 | Is 26:1-6/Mt 7:21, 24-27 |
| Friday, December 6, 2019 | Is 29:17-24/Mt 9:27-31 |
| Saturday, December 7, 2019 | Is 30:19-21, 23-26/Mt 9:35—10:1, 5a, 6-8 |
| Sunday, December 8, 2019 | Is 11:1-10/Rom 15:4-9/Mt 3:1-12 |
| Monday, December 9, 2019 | Gn 3:9-15, 20/Eph 1:3-6, 11-12/Lk 1:26-38 |
| Tuesday, December 10, 2019 | Is 40:1-11/Mt 18:12-14 |
| Wednesday, December 11, 2019 | Is 40:25-31/Mt 11:28-30 |
| Thursday, December 12, 2019 | Zec 2:14-17 or Rv 11:19a; 12:1-6a, 10ab/Lk 1:26-38 orLk1:39-47 |
| Friday, December 13, 2019 | Is 48:17-19/Mt 11:16-19 |
| Saturday, December 14, 2019 | Sir 48:1-4, 9-11/Mt 17:9a, 10-13 |
| Sunday, December 15, 2019 | Is 35:1-6a, 10/Jas 5:7-10/Mt 11:2-11 |
| Monday, December 16, 2019 | Nm 24:2-7, 15-17a/Mt 21:23-27 |
| Tuesday, December 17, 2019 | Gn 49:2, 8-10/Mt 1:1-17  |
| Wednesday, December 18, 2019 | Jer 23:5-8/Mt 1:18-25 |
| Thursday, December 19, 2019 | Jgs 13:2-7, 24-25a/Lk 1:5-25 |
| Friday, December 20, 2019 | Is 7:10-14/Lk 1:26-38 |
| Saturday, December 21, 2019 | Sg 2:8-14 or Zep 3:14-18a/Lk 1:39-45 |
| Sunday, December 22, 2019 | Is 7:10-14/Rom 1:1-7/Mt 1:18-24 |
| Monday, December 23, 2019 | Mal 3:1-4, 23-24/Lk 1:57-66 |
| Tuesday, December 24, 2019 | Sm 7:1-5, 8b-12, 14a, 16/Lk 1:67-79  |
| Wednesday, December 25, 2019 | Vigil: Is 62:1-5/Acts 13:16-17, 22-25/Mt 1:1-25 or 1:18-25 (13)Night: Is 9:1-6/Ti 2:11-14/Lk 2:1-14 (14)Dawn: Is 62:11-12/Ti 3:4-7/Lk 2:15-20 (15)Day: Is 52:7-10/Heb 1:1-6/Jn 1:1-18 or 1:1-5, 9-14 (16) |

## 2018

| December | Scripture |
| --- | --- |
| 18th | Psalms 9: 1-10 |
| 19th | Isaiah 2:2-5 |
| 20th | Isaiah 7:14-16 |
| 21st | Isaiah 9:1-7 |
| 22nd | Isaiah 11:1-9 |
| 23rd | Isaiah 25 |
| 24th | Isaiah 53:3-12 |

### Messiah Tag from Study

This was the input for the 2018 advent

- Isaiah 61:1–3

	1 The Spirit of the Lord God is upon me; because the Lord hath anointed me to preach good tidings unto the meek; he hath sent me to bind up the brokenhearted, to proclaim liberty to the captives, and the opening of the prison to them that are bound;

	2 To proclaim the acceptable year of the Lord, and the day of vengeance of our God; to comfort all that mourn;

	3 To appoint unto them that mourn in Zion, to give unto them beauty for ashes, the oil of joy for mourning, the garment of praise for the spirit of heaviness; that they might be called trees of righteousness, the planting of the Lord, that he might be glorified.

- Isaiah 1:16–18

    16 ¶ Wash you, make you clean; put away the evil of your doings from before mine eyes; cease to do evil;

    17 Learn to do well; seek judgment, relieve the oppressed, judge the fatherless, plead for the widow.

    18 Come now, and let us reason together, saith the Lord: though your sins be as scarlet, they shall be as white as snow; though they be red like crimson, they shall be as wool.

- Psalm 146:5–10

    5 Happy is he that hath the God of Jacob for his help, whose hope is in the Lord his God:

    6 Which made heaven, and earth, the sea, and all that therein is: which keepeth truth for ever:

    7 Which executeth judgment for the oppressed: which giveth food to the hungry. The Lord looseth the prisoners:

    8 The Lord openeth the eyes of the blind: the Lord raiseth them that are bowed down: the Lord loveth the righteous:

    9 The Lord preserveth the strangers; he relieveth the fatherless and widow: but the way of the wicked he turneth upside down.

    10 The Lord shall reign for ever, even thy God, O Zion, unto all generations. Praise ye the Lord.
    
- Psalm 113:1–9
  
    1 Praise ye the Lord. Praise, O ye servants of the Lord, praise the name of the Lord.

    2 Blessed be the name of the Lord from this time forth and for evermore.

    3 From the rising of the sun unto the going down of the same the Lord’s name is to be praised.

    4 The Lord is high above all nations, and his glory above the heavens.

    5 Who is like unto the Lord our God, who dwelleth on high,

    6 Who humbleth himself to behold the things that are in heaven, and in the earth!

    7 He raiseth up the poor out of the dust, and lifteth the needy out of the dunghill;

    8 That he may set him with princes, even with the princes of his people.

    9 He maketh the barren woman to keep house, and to be a joyful mother of children. Praise ye the Lord.
  
- Psalm 9:1–12
 
    1 I will praise thee, O Lord, with my whole heart; I will shew forth all thy marvellous works.

    2 I will be glad and rejoice in thee: I will sing praise to thy name, O thou most High.

    3 When mine enemies are turned back, they shall fall and perish at thy presence.

    4 For thou hast maintained my right and my cause; thou satest in the throne judging right.

    5 Thou hast rebuked the heathen, thou hast destroyed the wicked, thou hast put out their name for ever and ever.

    6 O thou enemy, destructions are come to a perpetual end: and thou hast destroyed cities; their memorial is perished with them.

    7 But the Lord shall endure for ever: he hath prepared his throne for judgment.

    8 And he shall judge the world in righteousness, he shall minister judgment to the people in uprightness.

    9 The Lord also will be a refuge for the oppressed, a refuge in times of trouble.

    10 And they that know thy name will put their trust in thee: for thou, Lord, hast not forsaken them that seek thee.

    11 Sing praises to the Lord, which dwelleth in Zion: declare among the people his doings.

    12 When he maketh inquisition for blood, he remembereth them: he forgetteth not the cry of the humble.
    
- Isaiah 53:3

    3 He is despised and rejected of men; a man of sorrows, and acquainted with grief: and we hid as it were our faces from him; he was despised, and we esteemed him not.

- Isaiah 25:8

    8 He will swallow up death in victory; and the Lord God will wipe away tears from off all faces; and the rebuke of his people shall he take away from off all the earth: for the Lord hath spoken it.

- Isaiah 11:1–9

    1 And there shall come forth a rod out of the stem of Jesse, and a Branch shall grow out of his roots:

    2 And the spirit of the Lord shall rest upon him, the spirit of wisdom and understanding, the spirit of counsel and might, the spirit of knowledge and of the fear of the Lord;

    3 And shall make him of quick understanding in the fear of the Lord: and he shall not judge after the sight of his eyes, neither reprove after the hearing of his ears:

    4 But with righteousness shall he judge the poor, and reprove with equity for the meek of the earth: and he shall smite the earth with the rod of his mouth, and with the breath of his lips shall he slay the wicked.

    5 And righteousness shall be the girdle of his loins, and faithfulness the girdle of his reins.

    6 The wolf also shall dwell with the lamb, and the leopard shall lie down with the kid; and the calf and the young lion and the fatling together; and a little child shall lead them.

    7 And the cow and the bear shall feed; their young ones shall lie down together: and the lion shall eat straw like the ox.

    8 And the sucking child shall play on the hole of the asp, and the weaned child shall put his hand on the cockatrice’ den.

    9 They shall not hurt nor destroy in all my holy mountain: for the earth shall be full of the knowledge of the Lord, as the waters cover the sea.

- Isaiah 2:2–5

    2 And it shall come to pass in the last days, that the mountain of the Lord’s house shall be established in the top of the mountains, and shall be exalted above the hills; and all nations shall flow unto it.

    3 And many people shall go and say, Come ye, and let us go up to the mountain of the Lord, to the house of the God of Jacob; and he will teach us of his ways, and we will walk in his paths: for out of Zion shall go forth the law, and the word of the Lord from Jerusalem.

    4 And he shall judge among the nations, and shall rebuke many people: and they shall beat their swords into plowshares, and their spears into pruninghooks: nation shall not lift up sword against nation, neither shall they learn war any more.

    5 O house of Jacob, come ye, and let us walk in the light of the Lord.
    
- Isaiah 53:4–12

    4 ¶ Surely he hath borne our griefs, and carried our sorrows: yet we did esteem him stricken, smitten of God, and afflicted.

    5 But he was wounded for our transgressions, he was bruised for our iniquities: the chastisement of our peace was upon him; and with his stripes we are healed.

    6 All we like sheep have gone astray; we have turned every one to his own way; and the Lord hath laid on him the iniquity of us all.

    7 He was oppressed, and he was afflicted, yet he opened not his mouth: he is brought as a lamb to the slaughter, and as a sheep before her shearers is dumb, so he openeth not his mouth.

    8 He was taken from prison and from judgment: and who shall declare his generation? for he was cut off out of the land of the living: for the transgression of my people was he stricken.

    9 And he made his grave with the wicked, and with the rich in his death; because he had done no violence, neither was any deceit in his mouth.

    10 ¶ Yet it pleased the Lord to bruise him; he hath put him to grief: when thou shalt make his soul an offering for sin, he shall see his seed, he shall prolong his days, and the pleasure of the Lord shall prosper in his hand.

    11 He shall see of the travail of his soul, and shall be satisfied: by his knowledge shall my righteous servant justify many; for he shall bear their iniquities.

    12 Therefore will I divide him a portion with the great, and he shall divide the spoil with the strong; because he hath poured out his soul unto death: and he was numbered with the transgressors; and he bare the sin of many, and made intercession for the transgressors.

- Isaiah 11:10–12
    
    10 ¶ And in that day there shall be a root of Jesse, which shall stand for an ensign of the people; to it shall the Gentiles seek: and his rest shall be glorious.

    11 And it shall come to pass in that day, that the Lord shall set his hand again the second time to recover the remnant of his people, which shall be left, from Assyria, and from Egypt, and from Pathros, and from Cush, and from Elam, and from Shinar, and from Hamath, and from the islands of the sea.

    12 And he shall set up an ensign for the nations, and shall assemble the outcasts of Israel, and gather together the dispersed of Judah from the four corners of the earth.
    
- Isaiah 9:1–7

    1 Nevertheless the dimness shall not be such as was in her vexation, when at the first he lightly afflicted the land of Zebulun and the land of Naphtali, and afterward did more grievously afflict her by the way of the sea, beyond Jordan, in Galilee of the nations.

    2 The people that walked in darkness have seen a great light: they that dwell in the land of the shadow of death, upon them hath the light shined.

    3 Thou hast multiplied the nation, and not increased the joy: they joy before thee according to the joy in harvest, and as men rejoice when they divide the spoil.

    4 For thou hast broken the yoke of his burden, and the staff of his shoulder, the rod of his oppressor, as in the day of Midian.

    5 For every battle of the warrior is with confused noise, and garments rolled in blood; but this shall be with burning and fuel of fire.

    6 For unto us a child is born, unto us a son is given: and the government shall be upon his shoulder: and his name shall be called Wonderful, Counsellor, The mighty God, The everlasting Father, The Prince of Peace.

    7 Of the increase of his government and peace there shall be no end, upon the throne of David, and upon his kingdom, to order it, and to establish it with judgment and with justice from henceforth even for ever. The zeal of the Lord of hosts will perform this.
    
- Isaiah 7:14–16

    14 Therefore the Lord himself shall give you a sign; Behold, a virgin shall conceive, and bear a son, and shall call his name Immanuel.

    15 Butter and honey shall he eat, that he may know to refuse the evil, and choose the good.

    16 For before the child shall know to refuse the evil, and choose the good, the land that thou abhorrest shall be forsaken of both her kings.

- Ezekiel 37:24–28

    24 And David my servant shall be king over them; and they all shall have one shepherd: they shall also walk in my judgments, and observe my statutes, and do them.

    25 And they shall dwell in the land that I have given unto Jacob my servant, wherein your fathers have dwelt; and they shall dwell therein, even they, and their children, and their children’s children for ever: and my servant David shall be their prince for ever.

    26 Moreover I will make a covenant of peace with them; it shall be an everlasting covenant with them: and I will place them, and multiply them, and will set my sanctuary in the midst of them for evermore.

    27 My tabernacle also shall be with them: yea, I will be their God, and they shall be my people.

    28 And the heathen shall know that I the Lord do sanctify Israel, when my sanctuary shall be in the midst of them for evermore.

# 2020 Advent

| Day                    | Scripture |
| ----------------------:| --------------- |
| Tuesday, December 1    | Isaiah 64       |
| Wednesday, December 2  | Romans 10:9-17  |
| Thursday, December 3   | Isaiah 2:2-5    |
| Friday, December 4     | Romans 13:10-14 |
| Saturday, December 5   | Isaiah 11:2-10  |
| Sunday, December 6     | Isaiah 25:6-9   |
| Monday, December 7     |  |
| Tuesday, December 8    |  |
| Wednesday, December 9  |  |
| Thursday, December 10  |  |
| Friday, December 11    |  |
| Saturday, December 12  |  |
| Sunday, December 13    |  |
| Monday, December 14    |  |
| Tuesday, December 15   |  |
| Wednesday, December 16 |  |
| Thursday, December 17  |  |
| Friday, December 18    |  |
| Saturday, December 19  |  |
| Sunday, December 20    |  |
| Monday, December 21    |  |
| Tuesday, December 22   |  |
| Wednesday, December 23 |  |
| Thursday, December 24  |  |
| Friday, December 25    |  |
| Saturday, December 26  |  |
| Sunday, December 27    |  |
